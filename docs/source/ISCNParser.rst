ISCNParser Core Functions
=============================

.. _ISCNParser

ISCNParser.parse_file
++++++++++++++++++++++

.. autofunction:: ISCNParser.parse_file

ISCNParser.parse_ISCN
++++++++++++++++++++++

.. autofunction:: ISCNParser.parse_ISCN

