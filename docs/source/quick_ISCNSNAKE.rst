Quickly Running ISCNSNAKE
===========================
.. _quick_SNAKE

| After installation, quick_SNAKE can be ran from any location using the following prompt entered in terminal or command prompt:
``python -m ISCNSNAKE.quick_SNAKE [-options]``

With no options specified, the program will ask for some user input for its mandatory parameters. These can also be specified by command line options.


| [-f] : Mitelman filters for selection rows and columns of the Mitelman database. See :doc:`Filters`
| [-p] : File suffix, determines names of output files. ex. -p Skeleton makes output files Skeleton_DeepDel.txt, Skeleton_DeepAmp.txt
| [-o] : Name of output folder, if it doesn't exist it will make one. Defaults to current directory.
| [-i] : Path to the plain text file of the Mitelman database

| Additionally, one optional input can be added:
| [-v] : Verbosity. If specified, will output each ISCN it comes across. Useful if you're unsure whether or not your filters were correct. Also useful if you want to see how fast this program works (its pretty fast) or just want to make sure the program is actually working. Additionally, if you have errors that crash the program the last ISCN to be printed to console is essential to determine what your issue is.


quick_SNAKE runs ISCNParser.parse_file() at the default settings, so if you want to change the handling of clones, filenames, or analyze aberration cooccurance you will have to interact with the menu provided by run_SNAKE.py

Example session:
+++++++++++++++++++

Consider the following example, where I am investigating Skeletal tumours; In particular, I want to better understand the most frequently gained/lost regions in a rare form of skeletal cancer called chordoma. In the Mitelman database, this type of case is defined by Topography : Skeleton and Morphology : Chordoma. There are 48 total patients with this designation.

To do this I will first enter in the command to start the program.

``python -m ISCNSNAKE.quick_SNAKE [-options]``

The console will then ask for input. 

``Enter as many filters as you want, separated by double forward slashes: ``

Since I am looking for Skeletal Chordoma I will enter the following: (Again, see :doc:`Filters` for how to make filter syntax.)

``Morphology:Chordoma//Topography:Skeleton``

A second prompt will be asked for, the prefix for the file. For organizational purposes we will use ``Skeleton``.

It will then ask if you want to use verbose mode. We will type ``y`` because we want to watch the cascade of ISCNs float down the screen. (But also because we want to make sure that we selected the right patients.

Finally, we are prompted to enter the path to the database file. Since we have it in plain text in the same folder we are running the script from, we will just type ``mitelmandatabase.txt`` (or whatever the name of the file is). If you are not in the same folder, for example you created a folder for the data in a folder below the database you will have to include the path to the file such as ../mitelmandatabase.txt (or ..\\mitelmandatabase.txt on windows).

This can all be done with one command with command line options. This would look like:

``python -m ISCNSNAKE.quick_SNAKE -f Morphology:Chordoma//Topography:Skeleton -p Skeleton -i mitelmandatabase.txt -v``

This will create the following files:
* Skeleton_Gain.txt - A file containing the relative frequency of single copy gains across all chromosomal regions
* Skeleton_Del.txt - A file containing the relative frequency of heterozygous deletions across all chromosomal regions
* Skeleton_DeepAmp.txt - A file containing the relative frequency of amplifications across all chromosomal regions
* Skeleton_DeepDel.txt - A file containing the relative frequency of homozygous deletions across all chromosomal regions
* Skeleton_AllQuantitative.txt - A file containing all analyzed patients in a copy number variation matrix
* Skeleton_recurrance.txt - A file containing the absolute frequency of individual cytogenetic events

