.. ISCNSNAKE documentation master file, created by
   sphinx-quickstart on Tue Aug  7 09:47:54 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ISCNSNAKE
=====================================

        ISCNSNAKE is a module developed using python to analyze numerical and quantitative aberrations found in the Mitelman Database of Chromosome Aberrations and Gene Fusions in Cancer.
        
        For a short explanation of the purpose and capability of this module, please read the introduction.
       
        For instructions on how to simply and quickly analyze subsets of cases in the Mitelman Database, go to :doc:`quick_ISCNSNAKE`.
        
        For usage of the full feature program, go to :doc:`ISCNSNAKE`.
        
        For instructions on how to use circos to visualize ISCNSNAKE output, please go to :doc:`Visualization`.

        For documentation of the source code of the core ISCNParser, please go to :doc:`ISCNParser`
        
.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   Introduction
   quick_ISCNSNAKE
   ISCNSNAKE
   Filters
   Visualization
   ISCNParser

* :ref:`genindex`
* :ref:`search`
